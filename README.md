# Braintri

Celem projektu było zbudowanie aplikacji serwerowej udostępniającej REST'owe API umożliwiającej
zarządzanie listą pracowników fikcyjnej firmy. Do zrealizowania projektu posłużyłem się framweworkami:
  * Django,
  * Django-REST.

Rozwiązanie wykorzystuje bazę SQLite - jest ona nieodpowiednia do celów produkcyjnych
ze względu na brak obsługi wielodostępowości, jednakże do prostego projektu dostępnego lokalnie
nadaje się znakomicie. Baza przechowywana jest w postaci jednego pliku db.sqlite3 dzięki czemu 
w łatwy sposób może być przenoszona.

Do uruchomienia wykorzystywany jest domyślny server *Django deployment server*.
Projektu, ze względu na jego charakter, nie opłaca się przenosić na inne rozwiązanie produkcyjne.

W celu uruchomienia serwera:
1. Utworzyć virtualenv
```buildoutcfg
virtualenv .venv
```

2. Uaktywnić virtualenv'a:
```buildoutcfg
source .venv/bin/activate
```

3. Zainstalować pakiety z pliku *requirements.txt*
```buildoutcfg
pip install -r requirements.txt
```

4. Włączyć serwer
```buildoutcfg
python manage.py runserver
```

Przez umieszczenie pliku db.sqlite3 w repozytorium niepotrzebne jest tworzenie / migrowanie bazy.
Nie jest to dobra praktyka, lecz w tym wypadku stosuję ją świadomie. 


Wymagane operacje biznesowe:
  * dodawanie nowego pracownika (wymagane atrybuty: imię, nazwisko, adres email),
  * zwracanie listy wszystkich pracowników z możliwością filtrowania po adresie email,
  * usuwanie danego pracownika z listy.
  
Zostały zrealizowane przy pomocy *ModelViewSet*. 


W celu chroniena prywatności danych autentykacja odbywa się poprzez token. 
Dla stworzonego użytkownika test o haśle 123test123, pobiera się go następująco:

######*W celu komunikacji z api wykorzystuje pakiet httpie*

```buildoutcfg
http POST 127.0.0.1:8000/api-token-auth/ username='test' password='123test123'
```

Dostępne operacje:

*%d* w adresach jest ID pracownika

1. W celu wyświetlanie listy wszystkich pracowników:
```buildoutcfg
http GET 127.0.0.1:8000/employees/ 'Authorization: Token b1118f2a632d33ba1abcb94f8300e9dabdfee8cc'
```

2. W celu przefiltrowania listy pracowników po adresie email:
```buildoutcfg
http GET 127.0.0.1:8000/employees/?email='tomek.miotk@gmail.com' 'Authorization: Token b1118f2a632d33ba1abcb94f8300e9dabdfee8cc'
```

3. W celu dodania nowego pracownika:
```buildoutcfg
http POST 127.0.0.1:8000/employees/ 'Authorization: Token b1118f2a632d33ba1abcb94f8300e9dabdfee8cc' first_name='test' last_name='test' email='test@test.com'
``` 

4. W celu modyfikacji danego pracownika:
```buildoutcfg
http PUT 127.0.0.1:8000/employees/%d/ 'Authorization: Token b1118f2a632d33ba1abcb94f8300e9dabdfee8cc' first_name='Krzysiek' last_name='Bak' email='test@test2.com'
```

5. W celu usunięcia danego pracownika:
```buildoutcfg
http DELETE 127.0.0.1:8000/employees/%d/ 'Authorization: Token b1118f2a632d33ba1abcb94f8300e9dabdfee8cc'
```
