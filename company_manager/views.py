# -*- coding: utf-8 -*-
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, generics

from company_manager.models import Employee
from company_manager.serializers import EmployeeSerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    """
    ModelViewSet providing retrieve, update and delete operations on Employee model.
    Additionally email field is filterable.
    """
    serializer_class = EmployeeSerializer
    queryset = Employee.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('email',)