# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib.admin import models
from django.db import models
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from rest_framework.authtoken.models import Token



@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    """
    Signal creating Token when new user was created.
    """
    if created:
        Token.objects.create(user=instance)


class Employee(models.Model):
    """
    Employee model.
    """
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField()

    def __unicode__(self):
        return "%s %s (%s)" % (self.first_name, self.last_name, self.email)
