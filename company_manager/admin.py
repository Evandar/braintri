from django.contrib import admin

from company_manager.models import Employee

admin.site.register(Employee)