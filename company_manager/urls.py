from django.conf.urls import url, include
from company_manager import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'employees', views.EmployeeViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]