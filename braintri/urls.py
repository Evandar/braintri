from django.conf.urls import url, include
from rest_framework.authtoken import views
from django.contrib import admin

urlpatterns = [
    url(r'^', include('company_manager.urls')),
    url(r'^api-token-auth/', views.obtain_auth_token),
    url(r'^admin/', admin.site.urls),
]
